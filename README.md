# Front-end MySoldi - HTML5 e CSS3 - Curso Algaworks

Projeto desenvolvido durante o **curso de Web Design Responsivo com HTML5, CSS3 e BEM da Algaworks** (https://www.algaworks.com/curso/web-design-responsivo-html5-css3-bem).

**Conceitos aplicados:**
* Design responsivo;
* Método mobile first;
* Sistema de grid do Bootstrap;
* Media queries;
* Menu hambúrguer com JavaScript, HTML5 e CSS3;
* Favicon;
* Listas UL;
* Flutuação de elementos com float;
* pseudo-elementos e pseudo-classes;
* position relative/absolute;
* Unidades de medidas: pixel, percentual, em e rem;
* Formulário com HTML5 e CSS3;

**Ferramentas utilizadas:**
* Visual Studio Code Versão 1.54.1;
* Navegador Google Chrome Versão 89.0.4389.82;
* Windows 10 Versão 1909 (OS Build 18363.1379);
* Bootstrap v3.3.5;

**Para visualizar as funcionalidades do site, baixe todos os arquivos em uma mesma pasta e abra o arquivo _index.html_ no seu navegador. Se preferir, na pasta _Apresentação Site_ tem algumas screenshots do site.**





